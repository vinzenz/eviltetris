#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <random>

static std::random_device random_device;
static std::default_random_engine random_engine(random_device());


struct BlockDefinition {
    int rotations;// How many different states are possible
    uint8_t const blocks[100];
    uint8_t widths[4];
    uint8_t heights[4];
};

BlockDefinition const BlockDefinitions[] = {
    {2,
    {1,0,0,0,0, 1,1,1,1,1, 1,0,0,0,0, 1,1,1,1,1,
     1,0,0,0,0, 0,0,0,0,0, 1,0,0,0,0, 0,0,0,0,0,
     1,0,0,0,0, 0,0,0,0,0, 1,0,0,0,0, 0,0,0,0,0,
     1,0,0,0,0, 0,0,0,0,0, 1,0,0,0,0, 0,0,0,0,0,
     1,0,0,0,0, 0,0,0,0,0, 1,0,0,0,0, 0,0,0,0,0,},
    {1, 5, 1, 5},
    {5, 1, 5, 1}},

    {4,
    {0,1,0,0,0, 0,1,0,0,0, 1,1,1,0,0, 1,0,0,0,0,
     1,1,1,0,0, 1,1,0,0,0, 0,1,0,0,0, 1,1,0,0,0,
     0,0,0,0,0, 0,1,0,0,0, 0,0,0,0,0, 1,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0},
     {3, 2, 3, 2},
     {2, 3, 2, 3}},

    {1,
    {1,1,0,0,0, 1,1,0,0,0, 1,1,0,0,0, 1,1,0,0,0,
     1,1,0,0,0, 1,1,0,0,0, 1,1,0,0,0, 1,1,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0},
     {2, 2, 2, 2},
     {2, 2, 2, 2}},

    {4,
    {1,1,0,0,0, 0,1,0,0,0, 1,1,0,0,0, 0,1,0,0,0,
     0,1,1,0,0, 1,1,0,0,0, 0,1,1,0,0, 1,1,0,0,0,
     0,0,0,0,0, 1,0,0,0,0, 0,0,0,0,0, 1,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0},
     {3, 2, 3, 2},
     {2, 3, 2, 3}},

    {4,
    {0,1,1,0,0, 1,0,0,0,0, 0,1,1,0,0, 1,0,0,0,0,
     1,1,0,0,0, 1,1,0,0,0, 1,1,0,0,0, 1,1,0,0,0,
     0,0,0,0,0, 0,1,0,0,0, 0,0,0,0,0, 0,1,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0},
     {3, 2, 3, 2},
     {2, 3, 2, 3}},

    {4,
    {0,0,1,0,0, 1,1,0,0,0, 1,1,1,0,0, 1,0,0,0,0,
     1,1,1,0,0, 0,1,0,0,0, 1,0,0,0,0, 1,0,0,0,0,
     0,0,0,0,0, 0,1,0,0,0, 0,0,0,0,0, 1,1,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0},
     {3, 2, 3, 2},
     {2, 3, 2, 3}},

    {4,
    {1,0,0,0,0, 0,1,0,0,0, 1,1,1,0,0, 1,1,0,0,0,
     1,1,1,0,0, 0,1,0,0,0, 0,0,1,0,0, 1,0,0,0,0,
     0,0,0,0,0, 1,1,0,0,0, 0,0,0,0,0, 1,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0,
     0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0},
     {3, 2, 3, 2},
     {2, 3, 2, 3}},
};
constexpr int const blockTypeCount = sizeof(BlockDefinitions) / sizeof(BlockDefinitions[0]);

struct Block {
    int       type;
    int       state;
    sf::Color color;
    int       x; // start point considered from origin
    int       y; // start point considered from origin
};
struct BlockState {
    sf::Color color;
    bool filled;
};

struct BlockRow {
    BlockState columns[10];
};

struct State {
    Block currentBlock;
    std::vector<BlockRow> field; // 23 lines, 10 Blocks per line
    sf::RenderWindow * window;
    int rowsRemoved;
};

void newBlock(State & state);

void clearState(State & state) {
    state.field.clear();
    state.field.resize(23);
    state.currentBlock = Block();
    state.rowsRemoved = 0;
    newBlock(state);
}

void drawBackground(State & state) {
    // auto size = window.getSize();
    // printf("Window size: %u/%u\n", size.x, size.y);
    sf::RectangleShape rect({316, 476});
    rect.setOutlineColor({0xcc, 0xcc, 0xcc});
    rect.setOutlineThickness(1);
    rect.setPosition({2,2});
    rect.setFillColor({0x44, 0x44, 0x44});
    state.window->draw(rect);
}

sf::Font & getFont() {
    static sf::Font * font = nullptr;
    if(!font) {
        font = new sf::Font();
        font->loadFromFile(
    #if defined(__APPLE__) && defined(__MACH__)
        "/Library/Fonts/Tahoma.ttf"
    #elif defined(__linux__)
        "/usr/share/fonts/dejavu/DejaVuSans.ttf"
    #endif
        ""
        );
    }
    return *font;
}

void drawRowCount(State & s) {
    sf::RectangleShape rect({s.window->getSize().x - 230.f, 30.f});
    rect.setOutlineColor({0x66, 0x66, 0x66});
    rect.setOutlineThickness(1);
    rect.setPosition({220, 10});
    rect.setFillColor(sf::Color::Transparent);
    s.window->draw(rect);
    std::ostringstream ostr;
    ostr << s.rowsRemoved << " Rows";
    sf::Text txt(ostr.str(), getFont(), 15);
    txt.setFillColor({0xcc, 0xcc, 0xcc});
    txt.setPosition({s.window->getSize().x - txt.getLocalBounds().width - 12, 15});
    s.window->draw(txt);
}

void drawAreaFrame(State & state) {
    sf::RectangleShape rect({201, 461});
    rect.setPosition({9, 9});
    rect.setOutlineColor({0x66, 0x66, 0x66});
    rect.setOutlineThickness(1);
    rect.setFillColor({0x22, 0x22, 0x22});
    state.window->draw(rect);
    for(int y = 0; y < 23; ++y) {
        for(int x = 0; x < 10; ++x) {
            if(state.field[y].columns[x].filled) {
                sf::RectangleShape rc({19,19});
                rc.setFillColor(state.field[y].columns[x].color);
                rc.setOutlineColor({0xaa,0xaa,0xaa});
                rc.setOutlineThickness(1);
                rc.setPosition(x * 20 + 10, y * 20 + 10);
                state.window->draw(rc);
            }
        }
    }
}

sf::Vector2i getBlockSize(State & state) {
    return {BlockDefinitions[state.currentBlock.type].widths[state.currentBlock.state],
            BlockDefinitions[state.currentBlock.type].heights[state.currentBlock.state]};
}

void drawGameOver(State & state) {

    sf::Text txt("GAME OVER", getFont(), 48);
    txt.setFillColor({0xcc, 0xcc, 0xcc});
    txt.setOutlineColor({0x22,0x22,0x22});
    txt.setOutlineThickness(.9);

    sf::RectangleShape rc({float(state.window->getView().getSize().x - 4), txt.getLocalBounds().height + 100});
    rc.setFillColor({0xaa,0xaa,0xaa});
    rc.setOutlineColor({246,218,83});
    rc.setOutlineThickness(2);
    rc.setPosition(2, (state.window->getSize().y - rc.getSize().y) / 2);

    txt.setPosition({(state.window->getSize().x - txt.getLocalBounds().width) / 2, ((state.window->getSize().y - txt.getLocalBounds().height) / 2) - txt.getLocalBounds().top});

    static bool blink = true;
    static int frameCount = 0;
    sf::Text instr("Press Enter to restart", getFont(), 21);
    instr.setFillColor(blink ? sf::Color{246,218,83} : sf::Color{0xff,0xff,0xff});
    ++frameCount;
    blink = (frameCount % 40) == 0 ? !blink : blink;
    instr.setOutlineThickness(.9);
    instr.setPosition({(state.window->getSize().x - instr.getLocalBounds().width) / 2, ((state.window->getSize().y - txt.getLocalBounds().height) / 2) - txt.getLocalBounds().top + txt.getLocalBounds().height});
    txt.move(0, (instr.getLocalBounds().height) / 2 * -1.f);
    instr.move(0, (instr.getLocalBounds().height / 2));

    state.window->draw(rc);
    state.window->draw(txt);
    state.window->draw(instr);
}

void drawBlock(State & state) {
    int offset = state.currentBlock.state * 5;
    auto maxSize = getBlockSize(state);
    uint8_t const * blocks = BlockDefinitions[state.currentBlock.type].blocks;
    for(int y = 0; y < maxSize.y; ++y) {
        for(int x = 0; x < maxSize.x; ++x) {
            int location = y * 20 + x + offset;
            if(blocks[location] != 0) {
                sf::RectangleShape rc({19,19});
                rc.setFillColor(state.currentBlock.color);
                rc.setOutlineColor({0xaa,0xaa,0xaa});
                rc.setOutlineThickness(1);
                rc.setPosition((state.currentBlock.x + x) * 20 + 10, (state.currentBlock.y + y) * 20 + 10);
                state.window->draw(rc);
            }
        }
    }
}

int getBlockWidth(State & state) {
    return getBlockSize(state).x;
}

int getBlockHeight(State & state) {
    return getBlockSize(state).y;
}

bool blockCollides(State & state) {
    int off = state.currentBlock.state * 5;
    uint8_t const * const b = BlockDefinitions[state.currentBlock.type].blocks;
    bool collides = false;
    for(int x = 0; x < 5; ++x) {
        for(int y = 0; y < 5; ++y) {
            if(b[off + x + y * 20]) {
                collides = state.field[(state.currentBlock.y + y)].columns[state.currentBlock.x + x].filled;
            }
            if(collides) {
                break;
            }
        }
        if(collides) {
            break;
        }
    }
    return collides;
}

bool atBottom(State & state) {
    bool result = false;
    state.currentBlock.y++;
    result = state.currentBlock.y > 23 - getBlockHeight(state) || blockCollides(state);
    state.currentBlock.y--;
    return result;
}

void applyBlock(State & state) {
    int off = state.currentBlock.state * 5;
    uint8_t const * const b = BlockDefinitions[state.currentBlock.type].blocks;
    for(int x = 0; x < 5; ++x) {
        for(int y = 0; y < 5; ++y) {
            if(b[off + x + y * 20]) {
                auto & field = state.field[state.currentBlock.y + y].columns[state.currentBlock.x + x];
                field.filled = true;
                field.color = state.currentBlock.color;
            }
        }
    }
}

static sf::Color const colors[] = {
    {163,130,169}, // #a382a9 - Purple
    {209,225,151}, // #d1e197 - Light green
    {166,44,97},   // #a62c61 - Dark Magenta
    {238,166,187}, // #eea6bb - Pink
    {30,144,122},  // #1e907a - Green
    {178,22,46},   // #b2162e - Red
    {246,218,83},  // #f6da53 - Yellow
    {16,38,78},    // #10264e - Dark Blue
    {188,205,239}, // #bccdef - Light Blue
    {203,70,2}     // #cb4602 - Orange
};
constexpr int const colorCount = sizeof(colors) / sizeof(colors[0]);

void rotateBlock(State & state) {
    int oldState = state.currentBlock.state;
    int widthBefore = getBlockWidth(state);
    state.currentBlock.state = (state.currentBlock.state + 1) % BlockDefinitions[state.currentBlock.type].rotations;
    // Ensure we're not crossing the border with the block after rotation.
    // If we are, move the block so far to the left that it fits
    state.currentBlock.x += (widthBefore - getBlockWidth(state)) / 2;
    if(state.currentBlock.x >= 10 - getBlockWidth(state)) {
        state.currentBlock.x = 10 - getBlockWidth(state);
    }
    else if(state.currentBlock.y >= 23 - getBlockHeight(state)) {
        state.currentBlock.state = oldState;
    }
    if(blockCollides(state)) {
        state.currentBlock.state = oldState;
    }
}

void newBlock(State & s) {
    static std::uniform_int_distribution<int> block_distribution(0, blockTypeCount - 1);
    static std::uniform_int_distribution<int> color_distribution(0, colorCount - 1);
    s.currentBlock = Block{
            block_distribution(random_engine),
            0,
            colors[color_distribution(random_engine)],
            0, 0};
    s.currentBlock.x = (10 - getBlockWidth(s)) / 2 + getBlockWidth(s) % 2;
}

void removeFullRows(State & s) {
    for(int y = 0; y < 23; ++y) {
        bool full = false;
        for(int x = 0; x < 10; ++x) {
            full = s.field[y].columns[x].filled;
            if(!full) {
                break;
            }
        }
        if(full) {
            s.rowsRemoved++;
            s.field.erase(s.field.begin() + y);
            s.field.insert(s.field.begin(), BlockRow());
        }
    }
}

int main()
{
    State state;
    state.rowsRemoved = 0;
    sf::RenderWindow window({320, 480}, "eviltetris");
    state.window = &window;
    clearState(state);
    window.setVerticalSyncEnabled(true);
    window.setFramerateLimit(60);
    bool atEnd = false;
    bool gameOver = false;
    while(window.isOpen()) {
        sf::Event event;
        while(window.pollEvent(event)) {
            if(event.type == sf::Event::Closed) {
                window.close();
            }
            if(!gameOver) {
                if(event.type == sf::Event::KeyPressed) {
                    switch(event.key.code) {
                    case sf::Keyboard::Left:
                        if(state.currentBlock.x > 0) {
                            --state.currentBlock.x;
                            if(blockCollides(state)) {
                                ++state.currentBlock.x;
                            }
                        }
                        break;
                    case sf::Keyboard::Right:
                        if(state.currentBlock.x < 10 - getBlockWidth(state)) {
                            ++state.currentBlock.x;
                            if(blockCollides(state)) {
                                --state.currentBlock.x;
                            }
                        }
                        break;
                    case sf::Keyboard::Space:
                        while(!atBottom(state)) {
                            ++state.currentBlock.y;
                        }
                        break;
                    case sf::Keyboard::Up:
                        // Rotate
                        rotateBlock(state);
                        break;
                    case sf::Keyboard::Down:
                        if(state.currentBlock.y < 23 - getBlockHeight(state)) {
                            ++state.currentBlock.y;
                            if(blockCollides(state)) {
                                --state.currentBlock.y;
                            }
                        }
                        break;
                    default:
                        // Ok
                        break;
                    }
                }
                if(atEnd) {
                    applyBlock(state);
                    newBlock(state);
                    if(atBottom(state)) {
                        gameOver = true;
                    }
                    atEnd = false;
                }
                atEnd = atBottom(state);
                removeFullRows(state);
            } else {
                if(event.type == sf::Event::KeyPressed) {
                    switch(event.key.code) {
                    case sf::Keyboard::Return:
                        atEnd = gameOver = false;
                        clearState(state);
                        break;
                    default:
                        break;
                    }
                }
            }
        }

        window.clear();
        drawBackground(state);
        drawAreaFrame(state);
        drawBlock(state);
        drawRowCount(state);
        if (gameOver) {
            drawGameOver(state);
        }
        window.display();
    }
    return 0;
}

