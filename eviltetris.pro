TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG -= qml_debug
QMAKE_CXXFLAGS += -std=c++11
macx {
    QMAKE_CXXFLAGS += -I/usr/local/Cellar/sfml/2.4.2_1/include
    LIBS += -L/usr/local/Cellar/sfml/2.4.2_1/lib 
}
LIBS += -lsfml-graphics -lsfml-window -lsfml-system
SOURCES += main.cpp

OTHER_FILES += \
    colors.html

